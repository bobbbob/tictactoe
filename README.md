# Tictactoe

Tic-tac-toe game implemented using the command line or the illwill library.

## Install and run
```sh
git clone https://codeberg.org/bobbbob/tictactoe.git
cd tictactoe
nimble run tictactoeillwill
# to run the command line version
nimble run tictactoe
```

## License
MIT
