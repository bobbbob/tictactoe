import illwill, os

proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)


const
    boardSize = 3
    vert1 = 7
    vert2 = 14
    vertend = 21
    horiz1 = 4
    horiz2 = 8
    horizend = 12
    wins = [
           [(0, 0), (0, 1), (0, 2)],
           [(1, 0), (1, 1), (1, 2)],
           [(2, 0), (2, 1), (2, 2)],
           [(0, 0), (1, 0), (2, 0)],
           [(0, 1), (1, 1), (2, 1)],
           [(0, 2), (1, 2), (2, 2)],
           [(0, 0), (1, 1), (2, 2)],
           [(0, 2), (1, 1), (2, 0)]
           ]


type Square = enum
    none, x, o

func `$` (s: Square): string =
    if s == none:
        return "-"
    elif s == x:
        return "X"
    else:
        return "O"


type Player = enum
    px, po

func `$` (p: Player): string =
    if p == px:
        return "X"
    else:
        return "O"

func getPlayerSquare(p: Player): Square =
    if p == Player.px:
        return Square.x
    else:
        return Square.o

func swap(self: var Player) =
    if self == Player.px:
        self = Player.po
    else:
        self = Player.px


type Board = array[boardSize, array[boardSize, Square]]

func initBoard(): Board =
    for i in result.mitems:
        for j in i.mitems:
            j = Square.none

func checkWin(self: Board, plr:Player): bool =
    let want = getPlayerSquare(plr)
    var score = 0
    for i in wins:
        score = 0
        for (row, col) in i:
            if self[row][col] == want:
                score += 1

        if score == 3:
            return true

    return false


type Game = object
    gboard: Board
    currentPlayer: Player
    turnsPassed: int
    gameOver: bool

func initGame(): Game =
    result.gboard = initBoard()
    result.currentPlayer = Player.px
    result.turnsPassed = 0
    result.gameOver = false

func setSquare(self: var Game, row, col: int) =
    if self.gboard[row][col] == Square.none:
        self.gboard[row][col] = getPlayerSquare(self.currentPlayer)
        self.currentPlayer.swap()
        self.turnsPassed += 1


proc updateScreen(tb: var TerminalBuffer, tgame: Game) =
    # setup grid
    tb.drawHorizLine(1, vertend - 1, horiz1)
    tb.drawHorizLine(1, vertend - 1, horiz2)
    tb.drawVertLine(vert1, 1, horizend - 1)
    tb.drawVertLine(vert2, 1, horizend - 1)

    # show grid squares
    tb.write(3, 2, $tgame.gboard[0][0])
    tb.write(11, 2, $tgame.gboard[0][1])
    tb.write(18, 2, $tgame.gboard[0][2])
    tb.write(3, 6, $tgame.gboard[1][0])
    tb.write(11, 6, $tgame.gboard[1][1])
    tb.write(18, 6, $tgame.gboard[1][2])
    tb.write(3, 10, $tgame.gboard[2][0])
    tb.write(11, 10, $tgame.gboard[2][1])
    tb.write(18, 10, $tgame.gboard[2][2])

    tb.write(1, 12, "Current player: ", $tgame.currentPlayer)




proc gameOverScreen(tb: var TerminalBuffer, tgame: Game) =
    # setup grid
    tb.drawHorizLine(1, vertend - 1, horiz1)
    tb.drawHorizLine(1, vertend - 1, horiz2)
    tb.drawVertLine(vert1, 1, horizend - 1)
    tb.drawVertLine(vert2, 1, horizend - 1)

    # show grid squares
    tb.write(3, 2, $tgame.gboard[0][0])
    tb.write(11, 2, $tgame.gboard[0][1])
    tb.write(18, 2, $tgame.gboard[0][2])
    tb.write(3, 6, $tgame.gboard[1][0]) #
    tb.write(11, 6, $tgame.gboard[1][1])
    tb.write(18, 6, $tgame.gboard[1][2])
    tb.write(3, 10, $tgame.gboard[2][0])
    tb.write(11, 10, $tgame.gboard[2][1])
    tb.write(18, 10, $tgame.gboard[2][2])

    # show who won
    if tgame.gboard.checkWin(Player.px):
        tb.write(1, 12, "Player X won.")
    elif tgame.gboard.checkWin(Player.po):
        tb.write(1, 12, "Player O won.")
    else:
        tb.write(1, 12, "Game was a tie.")
    tb.write(1, 13, "Press Q to quit.")


# returns tuple of if mouse is in a box, and its row and col if it is
proc getSpot(mi: MouseInfo): (bool, int, int) =
    if mi.x > 0 and mi.x < vert1 and mi.y > 0 and mi.y < horiz1:
        return (true, 0, 0)
    elif mi.x > vert1 and mi.x < vert2 and mi.y > 0 and mi.y < horiz1:
        return (true, 0, 1)
    elif mi.x > vert2 and mi.x < vertend and mi.y > 0 and mi.y < horiz1:
        return (true, 0, 2)
    elif mi.x > 0 and mi.x < vert1 and mi.y > horiz1 and mi.y < horiz2:
        return (true, 1, 0)
    elif mi.x > vert1 and mi.x < vert2 and mi.y > horiz1 and mi.y < horiz2:
        return (true, 1, 1)
    elif mi.x > vert2 and mi.x < vertend and mi.y > horiz1 and mi.y < horiz2:
        return (true, 1, 2)
    elif mi.x > 0 and mi.x < vert1 and mi.y > horiz2 and mi.y < horizend:
        return (true, 2, 0)
    elif mi.x > vert1 and mi.x < vert2 and mi.y > horiz2 and mi.y < horizend:
        return (true, 2, 1)
    elif mi.x > vert2 and mi.x < vertend and mi.y > horiz2 and mi.y < horizend:
        return (true, 2, 2)
    else:
        return (false, 0, 0)


proc gameLoop(self: var Game) =
    while not self.gameOver:
        var tb = newTerminalBuffer(terminalWidth(), terminalHeight())
        var key = getKey()
        case key
        of Key.Escape, Key.Q: exitProc()
        of Key.Mouse:
            let mi = getMouse()
            if mi.button == mbLeft:
                let (isInBox, row, col) = getspot(mi)
                if isInBox:
                    self.setSquare(row, col)
        else: discard

        if self.gboard.checkWin(px) or self.gboard.checkWin(po):
            self.gameOver = true
        elif self.turnsPassed == 9:
            self.gameOver = true

        tb.updateScreen(self)
        tb.display()
        sleep(50)

proc gameOverLoop(self: var Game) =
    while true:
        var tb = newTerminalBuffer(terminalWidth(), terminalHeight())
        var key = getKey()
        case key
        of Key.Escape, Key.Q: exitProc()
        else: discard

        tb.gameOverScreen(self)
        tb.display()
        sleep(50)


proc main() =
    illwillInit(fullscreen=true, mouse=true)
    setControlCHook(exitProc)
    hideCursor()

    var tgame = initGame()
    tgame.gameLoop()
    tgame.gameOverLoop()


when isMainModule:
    main()
    exitProc()



