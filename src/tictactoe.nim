import strformat, strutils


const
    boardSize = 3
    wins = [
           [(0, 0), (0, 1), (0, 2)],
           [(1, 0), (1, 1), (1, 2)],
           [(2, 0), (2, 1), (2, 2)],
           [(0, 0), (1, 0), (2, 0)],
           [(0, 1), (1, 1), (2, 1)],
           [(0, 2), (1, 2), (2, 2)],
           [(0, 0), (1, 1), (2, 2)],
           [(0, 2), (1, 1), (2, 0)]
           ]


type Square = enum
    none, x, o

func `$` (s: Square): string =
    if s == none:
        return "-"
    elif s == x:
        return "X"
    else:
        return "O"


type Player = enum
    px, po

func `$` (p: Player): string =
    if p == px:
        return "X"
    else:
        return "O"

func getPlayerSquare(p: Player): Square =
    if p == Player.px:
        return Square.x
    else:
        return Square.o

func swap(self: var Player) =
    if self == Player.px:
        self = Player.po
    else:
        self = Player.px


type Board = array[boardSize, array[boardSize, Square]]

func initBoard(): Board =
    for i in result.mitems:
        for j in i.mitems:
            j = Square.none

func checkWin(self: Board, plr:Player): bool =
    let want = getPlayerSquare(plr)
    var score = 0
    for i in wins:
        score = 0
        for (row, col) in i:
            if self[row][col] == want:
                score += 1

        if score == 3:
            return true

    return false

func `$` (self: Board): string =
    result.add("  {self[0][0]}  |  {self[0][1]}  |  {self[0][2]}\n".fmt)
    result.add("-----------------\n")
    result.add("  {self[1][0]}  |  {self[1][1]}  |  {self[1][2]}\n".fmt)
    result.add("-----------------\n")
    result.add("  {self[2][0]}  |  {self[2][1]}  |  {self[2][2]}\n".fmt)


type Game = object
    gboard: Board
    currentPlayer: Player
    turnsPassed: int
    gameOver: bool

func initGame(): Game =
    result.gboard = initBoard()
    result.currentPlayer = Player.px
    result.turnsPassed = 0
    result.gameOver = false

func setSquare(self: var Game, row, col: int) =
    if self.gboard[row][col] == Square.none:
        self.gboard[row][col] = getPlayerSquare(self.currentPlayer)
        self.currentPlayer.swap()
        self.turnsPassed += 1


proc gameLoop(self: var Game) =
    var
        rowInput: int
        colInput: int

    while true:
        echo self.gboard
        echo "Player ", $self.currentPlayer, ": "

        while true:
            try:
                stdout.write("Enter row: ")
                rowInput = stdin.readLine.parseInt            # rows 1 through 3
                if rowInput > boardSize or rowInput < 1:
                    echo "Inputs must be between 1 - 3"
                    continue

                stdout.write("Enter column: ")
                colInput = stdin.readLine.parseInt
                if colInput > boardSize or colInput < 1:
                    echo "Inputs must be between 1 - 3"
                    continue

                rowInput -= 1         # to change value into an index
                colInput -= 1

                if self.gboard[rowInput][colInput] != Square.none:
                    echo "Square is already set."
                    continue
            except ValueError:
                echo "Must enter a number."
                continue
            break

        self.setSquare(rowInput, colInput)

        if self.gboard.checkWin(self.currentPlayer):
            break
        if self.turnsPassed == 9:
            break

    echo self.gboard
    if self.gboard.checkWin(Player.px):
        echo "Player X won."
    elif self.gboard.checkWin(Player.po):
        echo "Player O won."
    else:
        echo "Game was a tie."

proc main() =
    var tgame = initGame()
    tgame.gameLoop()


when isMainModule:
    main()




