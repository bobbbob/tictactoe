# Package

version       = "0.1.0"
author        = "bobbbob"
description   = "Overengineered tic-tac-toe using the illwill library for learning purposes"
license       = "MIT"
srcDir        = "src"
bin           = @["tictactoe", "tictactoeillwill"]


# Dependencies

requires "nim >= 1.6.10"
requires "illwill >= 0.3.0"
